# makefile
#
PREFIX?=	/usr
DST=		${PREFIX}/jails/flavours
FLAVOURS=	cuba		

all:

install:
	@if test -d ${DST}; then\
		for i in ${FLAVOURS};\
		do cp -R -vp flavours/$${i} ${DST}; done; fi

uninstall:
	@if test -d ${DST}; then\
		for i in ${FLAVOURS};\
		do rm -vr ${DST}/$${i}; done; fi
