# README #

ezjail-flavour-cuba -- a flavour for jail w/ CUBA


# HOW TO USE

## 1. Install package ezjail
```
#!sh
sudo pkg install ezjail
```

## 2. Install flavour ezjail-flavour-cuba

```
#!sh
git clone https://oleg_kolobov@bitbucket.org/oleg_kolobov/ezjail-flavour-cuba.git
cd ezjail-flavour-cuba
sudo make install
```


## 3. Create jail cuba1

```
#!sh
sudo ezjail-admin create -f cuba cuba1 IP 
sudo ezjail-admin start cuba1
```

## 4. Get console of cuba1

```
#!sh
sudo ezjail-admin console cuba1
```


# SEE ALSO

* Information about flavours [ezjail main page](https://erdgeist.org/arts/software/ezjail/)
* Useful [supplements](https://github.com/libcrack/ezjail-flavours)



